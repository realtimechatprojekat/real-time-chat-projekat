
## O projektu

RealTimeChat aplikacija služi za međusobno instant komuniciranje putem
ASP.NET tehnologije između više korisnika. 

## Tehnologije projekta
```
*ASP.NET 4.6.1
*SignalR library
*Razor 
*SSMS Studio

*Jquery
*Knockout JS
*JS
```

## Značajne komande
```
cd existing_repo
git remote add origin https://gitlab.com/realtimechatprojekat/backup.git
git branch -M main
git push -uf origin main
```

## Kompatibilnost

Projekat najbolje radi na Firefox pretraživaču.
Na ostalim pretraživačima je neophodno obrisati cache memoriju.
